import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, Platform } from "ionic-angular";
import { Storage } from "@ionic/storage";
import { HttpClient } from "@angular/common/http";
import { ApiProvider } from "../../providers/api/api";

import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import { File } from "@ionic-native/file";
import { FileOpener } from "@ionic-native/file-opener";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

/**
 * Generated class for the ResultsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-results",
  templateUrl: "results.html"
})
export class ResultsPage {
  reports: any;
  loader: any;
  pdfObj = null;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private http: HttpClient,
    private api: ApiProvider,
    private plt: Platform,
    private file: File,
    private fileOpener: FileOpener
  ) {
    this.reports = [];
  }

  ionViewDidLoad() {
    this.getGenomeData();
  }

  getGenomeData() {
    this.storage.get("token").then(val => {
      const token = val;

      this.http
        .post(`${this.api.API_URL}patient`, { token: token })
        .subscribe(data => {
          this.filterReports(data);
        });
    });
  }

  filterReports(data) {
    let reportes = data["reports"].filter(item => {
      return item._data !== 404;
    });

    reportes = reportes.map(item => {
      return item._data;
    });

    this.reports[0] = reportes.filter(item => {
      return item.phenotype.category === "food_and_nutrition";
    });

    this.reports[1] = reportes.filter(item => {
      return item.phenotype.category === "personality";
    });

    this.reports[2] = reportes.filter(item => {
      return item.phenotype.category === "trait";
    });
  }

  getColorCode(code) {
    return `p${code}level`;
  }

  printPDF() {
    var resultados1 = this.reports[0].map(item => {
      let newItem = {};
      newItem["text"] = [];
      newItem["text"][0] = "\n" + item.phenotype.display_name + ": \n\t\t ";

      newItem["text"][1] = {};
      newItem["text"][1]["text"] =
        item.summary.text + " -  " + item.summary.score + "/4";

      newItem["text"][1]["style"] = this.getColorCode(item.summary.score);

      return newItem;
    });

    var resultados2 = this.reports[1].map(item => {
      let newItem = {};
      newItem["text"] = [];
      newItem["text"][0] = "\n" + item.phenotype.display_name + ": \n\t\t ";

      newItem["text"][1] = {};
      newItem["text"][1]["text"] =
        item.summary.text + " -  " + item.summary.score + "/4";

      newItem["text"][1]["style"] = this.getColorCode(item.summary.score);

      return newItem;
    });

    var resultados3 = this.reports[2].map(item => {
      let newItem = {};
      newItem["text"] = [];
      newItem["text"][0] = "\n" + item.phenotype.display_name + ": \n\t\t ";

      newItem["text"][1] = {};
      newItem["text"][1]["text"] =
        item.summary.text + " -  " + item.summary.score + "/4";

      newItem["text"][1]["style"] = this.getColorCode(item.summary.score);

      return newItem;
    });

    var imagen =
      "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAAZiS0dEAAAAAAAA+UO7fwAAAAlwSFlzAAAASAAAAEgARslrPgAAE49JREFUeNrtnXtwXFd9xz/n3LsPrZ6WbMtSbFmKJb/ysNRAYtIkdp2SkCaYQIgnLVBo0wBDB8owTKd/MNOhnc6U19Bp0weUkAcJhQAzCQWckCAsbIeExnGA+CH5Jclv62FZr33onnP6x9mVdle7sh67Wlnxd2bH1t57zz339/2d3/k9zrkryIJtW7b6gWZgO7AVaATKAMFVXA5h4CTwKvBjYHdr267BTCdOEua2LVsBbgA+DbwXqOaq0OeCIWAP8G/Ay61tu8aSDzrJf2zbstUFPgx8A7gTKOGq8OeKANCEVebShvr6fSe6OiOJg+MExIX/GeDLWK2/itwiANwKXNNQX/+rE12dYUgioKG+/sNY4ZcWuqeLGAK4EShuqK//5YmuTk8CbNuy9Qbg77kq/PmAAB4GHgQQcdPzKPCJQvfsbYbXgQ9IoAU7QVzF/GIT8EEJ3AfUFLo3b0P4gAclsIXF6moaDVrZ/0sJQtjvjC50zxJocbE+6iKDAUBV1hGrvwmvei06WIoYi+L0d+HvegPfuXZQYyBkITsadIHyQosrpzAG4w8RbtlOuPl+VHk1yKQBbmD0HTsItrcRevVpnEtnC0qCS1o0fEXDGEwgxPCWTxLedC9Ixw6GNItjikoJt9yHV1VH2QtfwenvLhgJBR1/OYcQhFvut8IXTsISTUaclLG6Gxne8nFMoJjsJ+cXi4cAo1GVdYSbt1vNT4aIP2m6q6EhumYz0TW3gi7MxLyICDBEG96JKl+RqswCZHgQf+d+3N5OJtkj10d07e3gBgrSbbdQ8so5HBdvxTqr5UkEiMgwpS/9C/4je9ChJQzd9Tlia26ZOMeAt7QBXVSGHO6zruo8YtGMACNddFGaQyfB7e3Ef+zXCC+GO3CGwJHdk8y98YcwviIKMQ8sGgKAjNor1JgNvITACIHwYpmvm2fNT2BxETAdLLCY/8qbA4wGY6ynIxLphfjfs5auwEhnok2wXpH24qMjf3p6BRGQSC+sIrb6JrzlTeiisniwZUBKvOWNMzfjBkyghOE7P42IhccJFbFR3L4ufN378Z3vyFva4sogIJFeaN5OuOV91tWUGbTdMKt51Lh+Yg03ZWxPhgcJtLcRevWZvKQtFj4B00wvzBlZ2tNFZYSb34tXtTovaYuFPwkLQbjl/ZdPL2S7PDaKSIpyRWTY2vbpIj6qJtIWJeTSXV3YBBiNV7WacMsU6YUpPiIWIdDeBiruegqJ79xhfGcOXvbazGmLdxFtzG3aYmGbIGOINdyMKquelF4Q4SHcnuMIlcGvRyCiIwSO7CbY3jZhMoRADvdT9uLXCDdvx6tcFT9mJl2vS5fhVa0iRUddl+jaOwge3jWzUTQFFjYBjou3Ym3G9ELZS1/Hf3Sv9YAyQGgVdyPTBrkQOH2dlLQ+inF8WW+tiysZevdnia3ZnJa2qEeHypFDvTkJ3hY0AVOmF46+ghiLTi2EbJNl/HuhxrJe6l48RbC9jdi1m1P7lJK2WOQECKMnpw6M1U5dugy3txNzOQKyHU/UijPCYBwfXtXqyaNPeTkzP7DACUCNWbePP0yWDaqilsG7P0+wow0xFslwoUCMhXHPHMC5dD6NBINxAnirNqLKa7IoscBbdi3h6++enNoeuoAMX8pZ7mhhE2AM/q59hFvujw/7CYzVbWKsbtMUVS+De+EIZS98Ffd8+7jZMY6fkds+Rrjl/RhfMPu90zTfXgz+ztcR0ZGcxQIL2w2VEt+pt/Aff3VyTy8X9QqBV7uWcMv7JlxYrfBWrCPcvB3jn0L4kMkxwunrJnjoF7l9xHkQ4xxgTUnx3qdwzx2deW8NqIqaFG9Hla/A+EMzi6XiVbWSPY/j9J98u0XCErf3OGU7v4S/cz+gJwKl9E/mBtL+nOLk9Pbi93H6T1H6868TaN/1NswFgSXhfAdlP/4i0XVbiDbdjqpcFbfhE8LUgRDIWTySMYjYSErKAhXDGerB3/k6wYMv4fR156Voc2UQACAkcnSAov3PEXzrRXSowpqSRD3A8TF052cYW3XdzBJ1cfNSuvNL8WynAANCxZCjAzZ3hMlbTeDKIQDGzYfwojiXzjFuyA3g+hCx0dm1qzzcvq7Jmc7xUmX+ymhXFgHJSBaMACPk9OSUafIVxIM2Oe8r5Bb+JDxnpAZhCw2LhgCbtoimfmlAlVejy6pt3ke6tmyZ7hgpz5YcC1Cxv3JNUDqUh3PxdOp3BlR5DYN3fY5g+y5UxTWEb7gnQ3qhBxkZKsiKicVDgNH4u/fbtEXaMsOxumbG6prj56VfB/7u/fH0wvwzsGhMEFLiO/k7/F37sqctMqUXBs4QOPQLCjU/LB4CEIjYCMV7n8DpPXn5JxMgoiMUv/KUXbR7dX9ADiAk7rl2yl74Mr7Th0CY1PpuUopBDvZQ2vofBA+8VLBliWD3CYeB4JxbWkgwGl26nMh1dxFtvBVVXovxBRDKQ4704eveT9Hvd+JeOEKh1youTgIgXis2mGApuqQK7Q8hvBhypB85OhBfsFt4A7B4vKB0JNIW0WGcyBDOeAlX5H2950ywMAhI7NsVTh7ssQGj7HwgMi34KSwKS4DR4AQQS9Yjq5oRJavAyfFWIeNhRs9j+n6L7vsdjOWunJgLFIgAY3Pw5Y04TX+GXLEZfMX5vaV6AN3zBurw45iLhxcMCfNPQFzr5co/xln7YURJ7axXNc8I0o9csRlRXIv3xj9j+g8sCBLmtwdGI0pW4m76HO6mz04If97uD6K0DnfjxyGwhNmtZY/PKSmf2a8VnZ8RYDRIH7J2C866jyLKG7KmBvJz/9T/i6obkCvehe76qZ34Z9CICFUjlqxHFK8E6WLCvZiBdsxQJ+iZb+LIPwFGIYqqkU0P4dTdA75QZsHrMUx/B2Y0qdI195sjgssQlden1oqlg1z+TvTJn09Pe40GXwlO/X3I1fchimsnlroYg4lewpx7BXX0e5ihrhmRkEcCDCCR1Ztx1n8MsWSDlXQG4ZtwL/rI91AnX7ReSi774CvBvekLyBW3pNxbhGrACYI3wpRDz2hEWQPOhoeRK26d2CAy3pZABCoQ9X+CqFiL9+ZXZjTJ54cAoyFQgXPtAzjX3g/+sixKrdEX9qMOfRtz8cAEI3MVukmSkPasnU7HVOtGE+0IB1l7B86GhxGldVM7CwZERSPOdZ/E+78vQmxgWs+SYwLidrLqBpz1f4Fc1gLIzCYnNoQ6/hzq+A8hOjAHj8SkBnJuMaJoGaKkDlG+BlGx3vYjfXN2pBdUNLOQEgq0ZodVIF/x9KyiAbm0GWflnahjP5xWUJk7AhJ2cvV9yMYdiKKqrAVwc7Eddfhx9PnfALPMyRgN0kEEKiFUiyi7FlnehChrgOIahL88yU5n4Kz3t3bnTPq9jUZUrMXZ+Ahy+TvIqkBexI4uXzEpJAqBrL0D1b0TvFEuNwpyQEAiqGrCWf8xG1RJN2un9cmfozqesZPtrFMDBrFkA079exFVNyCCy8ANTmhc0oaKSRBgBjvRZ3enNWlAushV77ZzVnHt1ArU8TQm0mtHevXNqecWX4MIVmGGLl9lmxsBRoMTtJ1e+yFEcU1W99IMnUJ1PIU+/cvMmjftexpkza04N/4NIlR9eYGn9YPYIOrwE5iRMxN9MBoRrEI2fQin/l5LZiYFUjH0yZdRHU/Z67WHPtWKXH5zUv9AOAE7wU8DsyfAaETJKpx1H0Fe80fg+LO4lx76zB7U4Scxg8fntvYmcc/rPmmFPx27nPy2sqFu1KHH0WcS+8ZMPC64HnfDI4ilm8jqqY1eQHV8B9394vjcISqvQ9a9J3UpuwCjouCF80TAeFC1FWf9RxFl9dm1PtyLPvp9VNdPc5YEk7V3THgkUwncGPCimGgfZuQ0pvdN9Jk2zPAp249ESqTuPThrP4IILZvCU3sDdfAxzMWD8U649vk3PJw5mh85hYn252ESNhoRqkY2PoSz+h5wi6bo9Juow9/G9L8VF0wOsh7Sh1iyPvvx2CBm5BTm0jH0pSN2xI2cxcQGJjweIW1wGFqBs+7Pkavuyj56x0ZQJ55DHX3WemoA/nKcpodwGu7PHFQajT6ze9oKNwMCBLL6Fpz1f4moXAcm81AlNoQ68bx1w6IXc5vwEmKKdLXBqAgm/uCiqBrhhjClDQhvGMZG7DFvBFFUbUdv5cbso3fwhDVX5/aO7wkT5Wusd1R9C9m8I31+H+rUy9Oua0yPAKORtXfgNn8eAuVTeAcdcffytfyU/LQHo+eysYMoWo4ILU/nJf6GFWVzNSpmNT5rSsRDn2pDtT+BGeq2XwoHWXM7zsaHs5s/AbrnTdTvH51RXDM9Ahw/su4eCJZPXvotAC+a5F6enUaUOUsYhT73KnLV3dm9jIzCiU/80gduKPN5AkykH33kf1CdP4n78NjYpvFBnDUPgK8ku3fU/SKq/UlMuCcPuSDhZHbNiHsHhx5Dn26dm3s5zX7onn2oE8/jNH5wVu+OyAyD6X0L79C3MH2/nbhd6WprcmpuBZwsjkaf9Y66fmbnmbxkQ70I5sI+WNpCatQHRC9ak5Nv4SegY6j2J0FFkfXbEcGKy8k2O+LBoer6GfrIM5hwYve7sLHGhkcmUucZrjX9B/EOfBPT92b8u5k//7QnYX3yBWTtbTarmRT4iIq1OKvvRR15Jn9CT39ybxTV/gT67B7k0k1QVI3wldi0gFuMcEPWQ3MCtuYs/TY6l27SW7biwWH7kzY4TGy+dkM4DR/AadqROYkYT53r7pdtQDce0c8O0zRBAhPuQR19Frfl71I9ESGQDdvRF17DDByZpzKf3ZZkBtpRA4cZdy9F4rVjPoTjAxmw7wN1iiwpvhC4JQhfCUbHML377UQrbCQlSlbatHPtlszmTYCJXER3PIPq+okNtub4vNN3Q4VEn30FXbMXuXJb6igIVeOs2YH35letpzFdGM2kteIw/W1BkxJpCpQCohgv0bSJdzOLHUmYnOqbcTZ+HFHROLWXd/Ab6J59me+fVwIQoCKoo88iqm5EFC1NIUHW3oE8uwd9Ztf0ynzGIErrEVXX26g12oeJDthgamwEVMSSmYmkyxIkkv6ZutiCE8Spfx/O2j+FQMUUrukvUYcex4yczukon1kkLCRmoB3d9VOcdR9NaymI0/QQpv8tTGSqMNwukJLXbLOhfHGt/VqPgYpivBFLQvQiRPoxkR47OUb6bHgfuxQnKGx3tRg1C4Ks8EVxjc1mrrwzewY3egl15HuoE89Z17Tg+4SNQXf+L7J6M2LJulRTtGQDcvW9qI7vZH1ofMU4ax7EadyRWuiQPptq8JdAqDpVfIa4eYlivFGIDWFiCYJ6IdyDSRAUvQRjQxgvDDoWfytKevAikcv+AGfjJxCV67NHw5eOoQ58E33hN+OKk2vMnAAhMOELqGM/wG35W5D+lGOyYTv6/GuYgfbUDhtt8y8b/srOIdl8+KxJNgfcEMIXgqKlCBpSj2sNKoZRozA2DNEBTLQfE+6DSI8lKtKHUVHk0mbkmgcQwSxFI6PQp3+FOvQYZjjxaoL8LNmYXTpaSPTZPeia25DXbE0dBUXLcBp34O3/stVA7O+2iMrrcK//VNzmz6HHWQmS4AYRbhCClVBalyoybWx/jLJB5VSl0qPPoo7/aF6WMc6yHiDAC09MyMHK1Am55jbk2b3o078A4SJX3omz8ZGJgk36Q3tRKxgnMPnlfFMJPRsyejFi6nWn8UqZOvjf6HOvkM/d8cmYfUFGSMzFQ3ZCXvuRtFaDOI07MIPH7KqCpoey5lHMYKddrxnuRRQtRQSXQtFyRHCpNRGBJeAvtX689NtfQ5qOwGcEhT7zCurgt+wCq3lcRT3HkmTShFzRNClCdm/+R6v1MkO+HYM+/zrqwH9iLh1Nk6OwI0H64wFUCQQqEIEqRNEyKFpmF1wFK63r6EsQ5JsZQQKIDaOO/wh17AcQG5r39aJzI0AIzOh5OyE3fz5tQnYQZauzZA/H0N07reZH+jPHDcZYr0dFINIHQ51pBLkTBPlLsxPkr7CelVNk09DxOYnYILr/ALrzORtYFWjHzNxXRQiBPrMbXXM7svb2SeswJwk/OojqeBp14vlpZg+zvNvfaFBhjApDpJfUWFfaEeQE4iOoDBGogGCVnQfGhjHDJ215UkUKunEjB8tS4smxo99HVF6PCCzJepoZOok68A1bZcLkoGaQnLpIg9HgjWLGRm2ckEkbCvByjnRIcvEabCEx/QfQ3TuzHDfonv14r/9D0nqc+dC4pP1gwkn7FH5vAFgCcrMa1mj0iecxg8dS9+UaD925E7XvnzADHQvmwRcKJNCdk5aExIyei7tyJ21UOnoBdfAxvN//64xLdW8TeC7wa+CmubYE2Aj5/K8xQ52I0Aqbnxk+xazXfy5+HJXAT4CB3LUpMCNn0D377GaFHL1jeRHCAM9LYC/wq5w2PT7pXdX6KdAJfFe2tu0axv6m/MVC9+htBAN8G/hdQkVbgX8H1KybvIqZ4EXgv1rbdtnfEj7R1Wka6uvfAFZgf2z+qtHOH14D/rq1bVcXJP2Y84muzmhDff1uIIAlwTe79q8iCzRW8z/V2rbrUOLLlCzYia7OSEN9/S6gHagDlrOYfnG7MDDYCfdrwBda23alxF1ZTc22LVtrgAeAHdg4IVToJ7nC4AEdwPPAd4GDrW27JqV9/h/1diMhB1+CbAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxOC0wOC0xOVQxNjo0MjozNCswMDowMGNAHzMAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTgtMDgtMTlUMTY6NDI6MzQrMDA6MDASHaePAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAABJRU5ErkJggg==";

    let docDefinition = {
      content: [
        {
          columns: [
            { image: imagen, width: 150 },
            {
              width: "*",
              text: [
                "Health Results \n\n",
                {
                  text: new Date().toTimeString(),
                  style: "subheader"
                }
              ],
              style: "header",
              alignment: "right"
            }
          ]
        },
        { text: "Food and nutrition \n\n", style: "subheader" },
        ...resultados1,
        { text: "Personality \n\n", style: "subheader" },
        ...resultados2,
        { text: "Trait \n\n", style: "subheader" },
        ...resultados3
      ],
      styles: {
        header: {
          fontSize: 24,
          bold: true,
          margin: [0, 0, 30, 0]
        },
        subheader: {
          alignment: "center",
          fontSize: 18,
          bold: true,
          margin: [0, 15, 30, 0]
        },
        p0level: {
          background: "#89d83f",
          fontSize: 14,
          margin: [0, 0, 30, 0]
        },
        p1level: {
          background: "#b5e630",
          fontSize: 14,
          margin: [0, 0, 30, 0]
        },

        p2level: {
          background: "#e6bd38",
          fontSize: 14,
          margin: [0, 0, 30, 0]
        },

        p3level: {
          background: "#f5963d",
          fontSize: 14,
          margin: [0, 0, 30, 0]
        },

        p4level: {
          background: "#e05353",
          fontSize: 14,
          margin: [0, 0, 30, 0]
        }
      }
    };

    this.pdfObj = pdfMake.createPdf(docDefinition);
  }

  downloadPdf() {
    if (this.plt.is("cordova")) {
      this.pdfObj.getBuffer(buffer => {
        var blob = new Blob([buffer], { type: "application/pdf" });

        // Save the PDF to the data Directory of our App
        this.file
          .writeFile(this.file.dataDirectory, "report.pdf", blob, {
            replace: true
          })
          .then(fileEntry => {
            // Open the PDf with the correct OS tools
            this.fileOpener.open(
              this.file.dataDirectory + "report.pdf",
              "application/pdf"
            );
          });
      });
    } else {
      // On a browser simply use download!
      this.pdfObj.download();
    }
  }
}
