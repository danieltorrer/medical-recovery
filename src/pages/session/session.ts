import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ResultsPage } from '../results/results';

/**
 * Generated class for the SessionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-session",
  templateUrl: "session.html"
})
export class SessionPage {
  populationTypes: any;
  selectedPopulation: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.populationTypes = [
      {
        type: "Mediterranean",
        image: "1.gif",
        value: "Mediterranean"
      },
      {
        type: "Northern European",
        image: "2.gif",
        value: "European"
      },
      {
        type: "Southwest Asian",
        image: "3.gif",
        value: "Asian"
      },
      {
        type: "Sub-Saharan African",
        image: "4.png",
        value: "African"
      },
      {
        type: "Southern African",
        image: "5.jpg",
        value: "African"
      },
      {
        type: "Northeast Asian",
        image: "6.jpg",
        value: "Asian"
      },
      {
        type: "Southeast Asian",
        image: "7.jpg",
        value: "Asian"
      },
      {
        type: "Native American",
        image: "8.jpg",
        value: "American"
      },
      {
        type: "Oceanian",
        image: "9.png",
        value: "Oceanian"
      }
    ];

    this.selectedPopulation = "";
  }

  displayResults() {
    this.navCtrl.push(ResultsPage);
  }

  assignValue(value) {
    this.selectedPopulation = value;
  }

}
