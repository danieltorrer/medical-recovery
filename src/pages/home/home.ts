import { Component, OnInit } from "@angular/core";
import { NavController } from "ionic-angular";
import { HttpClient } from "@angular/common/http";
import {
  InAppBrowser,
  InAppBrowserOptions
} from "@ionic-native/in-app-browser";
import { ApiProvider } from "../../providers/api/api";
import { AlertController } from "ionic-angular";
import { Storage } from "@ionic/storage";
import { SessionPage } from "../session/session";

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage implements OnInit {
  options: InAppBrowserOptions = {
    location: "yes", //Or 'no'
    hidden: "no", //Or  'yes'
    clearcache: "yes",
    clearsessioncache: "yes",
    zoom: "no", //Android only ,shows browser zoom controls
    hardwareback: "yes",
    mediaPlaybackRequiresUserAction: "no",
    shouldPauseOnSuspend: "no", //Android only
    closebuttoncaption: "Close", //iOS only
    disallowoverscroll: "no", //iOS only
    toolbar: "yes", //iOS only
    enableViewportScale: "no", //iOS only
    allowInlineMediaPlayback: "no", //iOS only
    presentationstyle: "pagesheet", //iOS only
    fullscreen: "yes" //Windows only
  };

  loggedIn: boolean;

  reports: any;

  constructor(
    public navCtrl: NavController,
    private http: HttpClient,
    private iab: InAppBrowser,
    private api: ApiProvider,
    public alertCtrl: AlertController,
    private storage: Storage
  ) {
    this.reports = [];
  }

  ngOnInit() {
    this.storage.get("token").then(val => {
      this.loggedIn = val ? true : false;
    });
  }

  connect() {
    this.http.get(`${this.api.API_URL}connect`).subscribe(data => {
      this.openWithCordovaBrowser(data["authorize_url"]);
    });
  }

  openWithSystemBrowser(url: string) {
    let target = "_system";
    this.iab.create(url, target, this.options);
  }

  openWithInAppBrowser(url: string) {
    let target = "_blank";
    this.iab.create(url, target, this.options);
  }

  openWithCordovaBrowser(url: string) {
    let target = "_self";
    let browser = this.iab.create(url, target, this.options);

    browser.on("exit").subscribe(data => {
      this.showPrompt();
    });
  }

  showPrompt() {
    const prompt = this.alertCtrl.create({
      title: "Enter code",
      message: "Paste your code here and hit OK",
      inputs: [
        {
          name: "code",
          placeholder: "Paste here"
        }
      ],
      buttons: [
        {
          text: "Cancel",
          handler: data => {}
        },
        {
          text: "OK",
          handler: data => {
            this.storage.set("token", data.code);
            this.loggedIn = true;
          }
        }
      ]
    });
    prompt.present();
  }

  newSession() {
    this.navCtrl.push(SessionPage);
  }

  logout() {
    this.storage.set("token", null);
    this.loggedIn = false;
  }
}
