import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {
  API_URL: string;

  constructor(public http: HttpClient) {
    this.API_URL = "https://medical-recovery.herokuapp.com/";
    // this.API_URL = "https://192.168.1.77.6:5000/";

    this.http
      .get("https://medical-recovery.herokuapp.com/")
      .subscribe(data => {});
  }
}
